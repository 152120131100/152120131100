#include "Matrix.h"
#include <iostream>
#include <iomanip>
using namespace std;
void Matrix_Allocate(Matrix& matrix, int rowSize, int columnSize) {


	int i;
	matrix.data = new float*[rowSize];
	for (i = 0; i < rowSize; i++) {
			matrix.data[i] = new float[columnSize];
	}
	matrix.rowSize = rowSize;
	matrix.columnSize = columnSize;
}
void Matrix_Free(Matrix& matrix) {
	

	int i, j;
	for (i = 0; i < matrix.rowSize; i++) {
			delete[] matrix.data[i];
	}
	delete[] matrix.data;
	matrix.rowSize = -1;
	matrix.columnSize = -1;
	matrix.data = nullptr;	
}

void Matrix_FillByValue(Matrix& matrix, float value) {
	
	int i, j;
	for (i = 0; i < matrix.rowSize; i++) {
			for (j = 0; j < matrix.columnSize; j++) {
					matrix.data[i][j] = value;
			}
	}


}
void Matrix_FillByData(Matrix & matrix, float ** data) {

	int i, j;
	for (i = 0; i < matrix.rowSize; i++) {
			for (j = 0; j < matrix.columnSize; j++) {
					matrix.data[i][j] = data[i][j];
			}
	}
}
void Matrix_Display(const Matrix& matrix) {
	

	int i, j;
	cout << "MATRIX : " << matrix.rowSize << " x " << matrix.columnSize <<
		endl;
	for (i = 0; i < matrix.rowSize; i++) {
			for (j = 0; j < matrix.columnSize; j++) {
					cout << setw(10) << matrix.data[i][j];
				cout << setw(10);
			}
		cout << "\n";
	}}
void Matrix_Addition(const Matrix & matrix_left, const Matrix & matrix_right,
	Matrix & result) {
	
	int i, j;
	Matrix_Allocate(result, matrix_left.rowSize,
		matrix_right.columnSize);
	for (i = 0; i < result.rowSize; i++) {
			for (j = 0; j < result.columnSize; j++) {
					result.data[i][j] = matrix_left.data[i][j] + matrix_right.data[i][j];
			}
	} 
}
void Matrix_Substruction(const Matrix & matrix_left,
	const Matrix & matrix_right, Matrix & result) {


	int i, j;
	Matrix_Allocate(result, matrix_left.rowSize,
		matrix_right.columnSize);
	for (i = 0; i < result.rowSize; i++) {
			for (j = 0; j < result.columnSize; j++) {
					result.data[i][j] = matrix_left.data[i][j] -
					matrix_right.data[i][j];
			}
	}
}
void Matrix_Multiplication(const Matrix & matrix_left,
	const Matrix & matrix_right, Matrix & result) {

	int i, j, k;
	Matrix_Allocate(result, matrix_left.rowSize,
		matrix_right.columnSize);
	for (i = 0; i < matrix_left.rowSize; i++) {
			for (j = 0; j < matrix_right.columnSize; j++) {
					result.data[i][j] = 0;
				for (k = 0; k < matrix_left.columnSize; k++) {
						result.data[i][j] = result.data[i][j] +
						(matrix_left.data[i][k] * matrix_right.data[k][j]);
				}
			}
	}
}
void Matrix_Multiplication(const Matrix & matrix_left, float scalarValue,
	Matrix & result) {
	
	int i, j;
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
		for (i = 0; i < result.rowSize; i++) {
				for (j = 0; j < result.columnSize; j++) {
						result.data[i][j] = matrix_left.data[i][j] * scalarValue;
				}
		}
}
void Matrix_Division(const Matrix & matrix_left, float scalarValue,
	Matrix & result) {

	int i, j;
	Matrix_Allocate(result, matrix_left.rowSize, matrix_left.columnSize);
		for (i = 0; i < result.rowSize; i++) {
				for (j = 0; j < result.columnSize; j++) {
						result.data[i][j] = matrix_left.data[i][j] /
						scalarValue;
				}
		}
}